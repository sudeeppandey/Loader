package pandey.sudeep.myapplication;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class MoviesProvider extends ContentProvider {

    public static final String AUTHORITY = "pandey.sudeep.myapplication";

    public static final Uri URI_MOVIES = Uri.parse(
            "content://" + AUTHORITY + "/" + "bestmovies");

    private static final int CODE_MOVIES_DIR = 1;
    private static final int CODE_MOVIES_ITEM = 2;

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        MATCHER.addURI(AUTHORITY, "bestmovies", CODE_MOVIES_DIR);
        MATCHER.addURI(AUTHORITY, "bestmovies" + "/*", CODE_MOVIES_ITEM);
    }

    public MoviesProvider() {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public String getType(Uri uri) {
        switch (MATCHER.match(uri)) {
            case CODE_MOVIES_DIR:
                return "vnd.android.cursor.dir/" + AUTHORITY + "." + "bestmovies";
            case CODE_MOVIES_ITEM:
                return "vnd.android.cursor.item/" + AUTHORITY + "." + "bestmovies";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public boolean onCreate() {

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        final int code = MATCHER.match(uri);
        if(code==CODE_MOVIES_DIR||code==CODE_MOVIES_ITEM){
            final Context context = getContext();
            if(context==null){
                return null;
            }
            //AppExecutors executors = new AppExecutors();
            final MovieDAO dao = AppDatabase.getInstance(context).getMovieDAO();

            Cursor cursor=null;
            if(code==CODE_MOVIES_DIR){
                cursor = dao.loadAllMoviesForCursor(selectionArgs);
            }else if(code==CODE_MOVIES_ITEM){
                throw new IllegalArgumentException("Invalid URI" + uri);
            }
            return cursor;
        }else{
            throw new IllegalArgumentException("Unknown URI: "+uri);
        }

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        return 0;
    }
}
