package pandey.sudeep.myapplication;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.VisibleForTesting;

@Database(entities={BestMovies.class},version=1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase sInstance;

    @VisibleForTesting
    public static final String DATABASE_NAME = "sample-db";

    public abstract MovieDAO getMovieDAO();

    public static synchronized AppDatabase getInstance(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).build();
            sInstance.populateInitialData();
        }
        return sInstance;
    }
    private void populateInitialData(){
        if(sInstance.getMovieDAO().countRecords()==0) {
            sInstance.beginTransaction();
            try {
                sInstance.getMovieDAO().insertAllMovies(DataGenerator.generateMovies());
                sInstance.setTransactionSuccessful();
            } finally {
                sInstance.endTransaction();
            }
        }
    }
}

