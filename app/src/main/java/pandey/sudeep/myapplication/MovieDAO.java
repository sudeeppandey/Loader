package pandey.sudeep.myapplication;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

@Dao
public interface MovieDAO {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertAllMovies(List<BestMovies> myMovies);

    @Query("Select * from bestmovies")
    List<BestMovies> loadAllMovies();

    @Query("Select count(*) from bestmovies")
    int countRecords();

    @Query("Select * from bestmovies where genre IN (:genres)")
    Cursor loadAllMoviesForCursor(String[] genres);

    @Query("Select * from bestmovies where genre=:genre")
    List<BestMovies> loadMoviesForGenre(String genre);

    @Query("Select * from bestmovies where genre=:genre")
    Cursor loadMoviesByGenreForCursor(String genre);

    @Update
    int updateMovie(BestMovies movie);

    @Query("Delete from bestmovies where id= :movieToDelete")
    int deleteMovie(int movieToDelete);

}
