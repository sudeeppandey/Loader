package pandey.sudeep.myapplication;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder>{

    private List<BestMovies> movies;

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;

        public ViewHolder(CardView cv){
            super(cv);
            this.cardView=cv;
        }
    }

    public MovieAdapter(List<BestMovies> _movies){
        this.movies=_movies;
    }

    @Override
    public int getItemCount(){
        return movies.size();
    }

    @Override
    public MovieAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final CardView cardView = holder.cardView;
        TextView textView1 = (TextView)cardView.findViewById(R.id.textView);
        textView1.setText(Long.toString(movies.get(position).getId()));
        TextView textView2 = (TextView)cardView.findViewById(R.id.textView2);
        textView2.setText(movies.get(position).getMovieName().toString());

    }
}
