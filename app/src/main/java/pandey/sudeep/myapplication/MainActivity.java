package pandey.sudeep.myapplication;

import android.app.LoaderManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    private AppDatabase database;
    private Context context = this;
    private MovieAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private List<BestMovies> movies;
    private String userInput;

    private static final String TAG = MainActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getIntent() != null) {
            handleIntent(getIntent());
        }

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        ((Button)findViewById(R.id.button2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(movies.size()!=0){
                    movies.clear();
                    //adapter.notifyDataSetChanged();

                }*/
                //getLoaderManager().initLoader(0,null,MainActivity.this);
            }
        });

        final AppExecutors executors = new AppExecutors();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                database = AppDatabase.getInstance(context);
            }
        });


        Button basicLoad = (Button)findViewById(R.id.button);
        basicLoad.setOnClickListener(new View.OnClickListener() {

            /**
             * need to use AsyncTask for the below implementation.
             * However, it does seem that 'adapter' variable is
             * properly instantiated in different thread before the
             * recyclerview object gets the adapter in UI thread.
             */

            @Override
            public void onClick(View v) {
                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        movies = AppDatabase.getInstance(context).getMovieDAO().loadAllMovies();
                        adapter = new MovieAdapter(movies);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView = findViewById(R.id.recyclerView);
                                recyclerView.setHasFixedSize(true);
                                linearLayoutManager = new LinearLayoutManager(context);
                                recyclerView.setLayoutManager(linearLayoutManager);
                                recyclerView.setAdapter(adapter);
                            }
                        });

                    }
                });
            }
        });

    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.options_menu, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    private void handleIntent(Intent intent) {
        // Special processing of the incoming intent only occurs if the if the action specified
        // by the intent is ACTION_SEARCH.
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // SearchManager.QUERY is the key that a SearchManager will use to send a query string
            // to an Activity.
            String query = intent.getStringExtra(SearchManager.QUERY);
           //((Button)findViewById(R.id.button)).setText(query);
            userInput = query;
            getLoaderManager().initLoader(0,null,MainActivity.this);

        }
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Uri uri = MoviesProvider.URI_MOVIES;
        String[] projection = new String[]{"id","movieName","genre"};
        //String selection = "genre = ?";
        String selection = null;
        String[] selectionArgs = new String[]{userInput};
        CursorLoader loader = new CursorLoader(context,uri,projection,selection,selectionArgs,null);

        return loader;
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Swap the new cursor in.  (The framework will take care of closing the
        // old cursor once we return.)
        if(movies!=null) {
            movies.clear();
        }
        if (data.moveToFirst()){
            while(!data.isAfterLast()){
                long Id = data.getLong(0);
                String movie = data.getString(1);
                String genre = data.getString(2);
                movies.add(new BestMovies(Id,movie,genre));
                // do what ever you want here
                data.moveToNext();
            }
        }
        //Log.d(TAG, Integer.toString(data.getCount()));
        //movies.clear();
        adapter.notifyDataSetChanged();
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        // This is called when the last Cursor provided to onLoadFinished()
        // above is about to be closed.  We need to make sure we are no
        // longer using it.

    }


}
